﻿using System;
using Trinity.Framework;
using Trinity.Framework.Helpers;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using Trinity.Framework.Reference;
using System.Threading;
using System.Windows;
using System.Xml;
using System.Xml.Linq;
using Trinity.Framework.Objects;
using Zeta.Bot.Settings;
using Zeta.Game;
using Trinity.Framework;
using Trinity.Settings;

namespace Trinity.Settings
{
    [DataContract(Namespace = "")]
    public class SenExtendSettings : NotifyBase
    {
        public SenExtendSettings()
        {
            LoadDefaults();
        }

        public override void LoadDefaults()
        {
            RandMiniGreaterLevel = 50;
            RandMaxGreaterLevel = 60;
            return;
        }

        #region 变量
        /// <summary>
        /// 打完BOSS后继续找粪池
        /// </summary>
        private bool _keepKillingAfterBoss;

        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool KeepKillingAfterBoss
        {
            get { return _keepKillingAfterBoss; }
            set
            {
                if (_keepKillingAfterBoss != value)
                {
                    _keepKillingAfterBoss = value;
                    OnPropertyChanged("KeepKillingAfterBoss");
                }
            }
        }

        /// <summary>
        /// 启动自定义经验池经验限制
        /// </summary>
        private bool _enableNephalemRestExperienceCheck;

        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool EnableNephalemRestExperienceCheck
        {
            get { return _enableNephalemRestExperienceCheck; }
            set
            {
                if (_enableNephalemRestExperienceCheck != value)
                {
                    _enableNephalemRestExperienceCheck = value;
                    OnPropertyChanged("EnableNephalemRestExperienceCheck");
                }
            }
        }

        /// <summary>
        /// 经验池经验值
        /// </summary>
        private int _miniNormalRiftForXPShrine;

        [DataMember(IsRequired = false)]
        [DefaultValue(50)]
        public int MiniNormalRiftForXPShrine
        {
            get { return _miniNormalRiftForXPShrine; }
            set
            {
                if (_miniNormalRiftForXPShrine != value)
                {
                    _miniNormalRiftForXPShrine = value;
                    OnPropertyChanged("MiniNormalRiftForXPShrine");
                }
            }
        }

        /// <summary>
        /// 启动大秘境随机层数
        /// </summary>
        private bool _enableRandGreaterLevel;

        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool EnableRandGreaterLevel
        {
            get { return _enableRandGreaterLevel; }
            set
            {
                if (_enableRandGreaterLevel != value)
                {
                    _enableRandGreaterLevel = value;
                    OnPropertyChanged("EnableRandGreaterLevel");
                }
            }
        }

        /// <summary>
        /// 随机大秘层数(最小)
        /// </summary>
        private int _randMiniGreaterLevel;

        [DataMember(IsRequired = false)]
        public int RandMiniGreaterLevel
        {
            get { return _randMiniGreaterLevel; }
            set
            {
                if (_randMiniGreaterLevel != value)
                {
                    _randMiniGreaterLevel = value;
                    OnPropertyChanged("RandMiniGreaterLevel");
                }
            }
        }

        public string RandMiniGreaterLevelRaw
        {
            get
            {
                return RandMiniGreaterLevel.ToString();
            }
            set
            {
                int randMiniGreaterLevel;
                if (int.TryParse(value, out randMiniGreaterLevel))
                {
                    RandMiniGreaterLevel = randMiniGreaterLevel;
                }
            }
        }



        /// <summary>
        /// 随机大秘层数(最大)
        /// </summary>
        private int _randMaxGreaterLevel;

        [DataMember(IsRequired = false)]
        public int RandMaxGreaterLevel
        {
            get { return _randMaxGreaterLevel; }
            set
            {
                if (_randMaxGreaterLevel != value)
                {
                    _randMaxGreaterLevel = value;
                    OnPropertyChanged("RandMaxGreaterLevel");
                }
            }
        }

        public string RandMaxGreaterLevelRaw
        {
            get
            {
                return RandMaxGreaterLevel.ToString();
            }
            set
            {
                int randMaxGreaterLevel;
                if (int.TryParse(value, out randMaxGreaterLevel))
                {
                    RandMaxGreaterLevel = randMaxGreaterLevel;
                }
            }
        }

        [IgnoreDataMember]
        public List<string> GreaterRiftLevels
        {
            get
            {
                var unlockedRiftLevel = 150;

                var levels = new List<string>();
                for (var i = 1; i <= unlockedRiftLevel; i++)
                {
                    levels.Add(i.ToString());
                }
                return levels;
            }
        }



        /// <summary>
        /// 开启智能整理包裹
        /// </summary>
        private bool _enableIntelligentFinishing;

        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool EnableIntelligentFinishing
        {
            get { return _enableIntelligentFinishing; }
            set
            {
                if (_enableIntelligentFinishing != value)
                {
                    _enableIntelligentFinishing = value;
                    OnPropertyChanged("EnableIntelligentFinishing");
                }
            }
        }

        /// <summary>
        /// 开启智能整理包裹 - 城镇外包裹数量
        /// </summary>
        private int _freeBagSlots;

        [DataMember(IsRequired = false)]
        [DefaultValue(2)]
        public int FreeBagSlots
        {
            get { return _freeBagSlots; }
            set
            {
                if (_freeBagSlots != value)
                {
                    _freeBagSlots = value;
                    OnPropertyChanged("FreeBagSlots");
                }
            }
        }

        /// <summary>
        /// 开启智能整理包裹 - 城镇内包裹数量
        /// </summary>
        private int _freeBagSlotsInTown;

        [DataMember(IsRequired = false)]
        [DefaultValue(30)]
        public int FreeBagSlotsInTown
        {
            get { return _freeBagSlotsInTown; }
            set
            {
                if (_freeBagSlotsInTown != value)
                {
                    _freeBagSlotsInTown = value;
                    OnPropertyChanged("FreeBagSlotsInTown");
                }
            }
        }
        /// <summary>
        /// 小秘是否拾取奥术之尘
        /// </summary>
        private bool _isPickArcaneDust;

        [DataMember(IsRequired = false)]
        [DefaultValue(true)]
        public bool IsPickArcaneDust
        {
            get { return _isPickArcaneDust; }
            set
            {
                if (_isPickArcaneDust != value)
                {
                    _isPickArcaneDust = value;
                    OnPropertyChanged("IsPickArcaneDust");
                }
            }
        }

        /// <summary>
        /// 小秘是否拾取死亡之息
        /// </summary>
        private bool _isPickDeathsBreath;

        [DataMember(IsRequired = false)]
        [DefaultValue(true)]
        public bool IsPickDeathsBreath
        {
            get { return _isPickDeathsBreath; }
            set
            {
                if (_isPickDeathsBreath != value)
                {
                    _isPickDeathsBreath = value;
                    OnPropertyChanged("IsPickDeathsBreath");
                }
            }
        }

        /// <summary>
        /// 小秘是否拾取遗忘之魂
        /// </summary>
        private bool _isPickForgottenSoul;

        [DataMember(IsRequired = false)]
        [DefaultValue(true)]
        public bool IsPickForgottenSoul
        {
            get { return _isPickForgottenSoul; }
            set
            {
                if (_isPickForgottenSoul != value)
                {
                    _isPickForgottenSoul = value;
                    OnPropertyChanged("IsPickForgottenSoul");
                }
            }
        }

        /// <summary>
        /// 小秘是否拾取万用材料
        /// </summary>
        private bool _isPickReusableParts;

        [DataMember(IsRequired = false)]
        [DefaultValue(true)]
        public bool IsPickReusableParts
        {
            get { return _isPickReusableParts; }
            set
            {
                if (_isPickReusableParts != value)
                {
                    _isPickReusableParts = value;
                    OnPropertyChanged("IsPickReusableParts");
                }
            }
        }

        /// <summary>
        /// 小秘是否拾取萦雾水晶
        /// </summary>
        private bool _isPickVeiledCrystal;
        
        [DataMember(IsRequired = false)]
        [DefaultValue(true)]
        public bool IsPickVeiledCrystal
        {
            get { return _isPickVeiledCrystal; }
            set
            {
                if (_isPickVeiledCrystal != value)
                {
                    _isPickVeiledCrystal = value;
                    OnPropertyChanged("IsPickVeiledCrystal");
                }
            }
        }

        /// <summary>
        /// 小米开箱,大米忽略
        /// </summary>
        private bool _ignoreBoxInGreaterRift;

        [DataMember(IsRequired = false)]
        [DefaultValue(true)]
        public bool IgnoreBoxInGreaterRift
        {
            get { return _ignoreBoxInGreaterRift; }
            set
            {
                if (_ignoreBoxInGreaterRift != value)
                {
                    _ignoreBoxInGreaterRift = value;
                    OnPropertyChanged("IgnoreBoxInGreaterRift");
                }
            }
        }

        /// <summary>
        /// 小秘忽略所有躲避
        /// </summary>
        private bool _ignoreAvoidanceInNephalemRift;

        [DataMember(IsRequired = false)]
        [DefaultValue(true)]
        public bool IgnoreAvoidanceInNephalemRift
        {
            get { return _ignoreAvoidanceInNephalemRift; }
            set
            {
                if (_ignoreAvoidanceInNephalemRift != value)
                {
                    _ignoreAvoidanceInNephalemRift = value;
                    OnPropertyChanged("IgnoreAvoidanceInNephalemRift");
                }
            }
        }

        /// <summary>
        /// 进度球拾取控制(小秘境)
        /// </summary>
        private bool _firstPickupNephalemRift;

        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool FirstPickupNephalemRift
        {
            get { return _firstPickupNephalemRift; }
            set
            {
                if (_firstPickupNephalemRift != value)
                {
                    _firstPickupNephalemRift = value;
                    OnPropertyChanged("FirstPickupNephalemRift");
                }
            }
        }

        /// <summary>
        /// 进度球拾取控制(大秘境)
        /// </summary>
        private bool _firstPickupGreaterRift;

        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool FirstPickupGreaterRift
        {
            get { return _firstPickupGreaterRift; }
            set
            {
                if (_firstPickupGreaterRift != value)
                {
                    _firstPickupGreaterRift = value;
                    OnPropertyChanged("FirstPickupGreaterRift");
                }
            }
        }

        /// <summary>
        /// 加强防漏经验池
        /// </summary>
        private bool _preventExpPool;

        [DataMember(IsRequired = false)]
        [DefaultValue(true)]
        public bool PreventExpPool
        {
            get { return _preventExpPool; }
            set
            {
                if (_preventExpPool != value)
                {
                    _preventExpPool = value;
                    OnPropertyChanged("IgnoreAvoidanceInNephalemRift");
                }
            }
        }

        #endregion

        #region 模式设置
        /// <summary>
        /// 更换指定装备
        /// </summary>
        private bool _ChangeEquipmentByItem;
        [DataMember(IsRequired = true)]
        [DefaultValue(true)]
        public bool ChangeEquipmentByItem
        {
            get { return _ChangeEquipmentByItem; }
            set { SetField(ref _ChangeEquipmentByItem, value); }
        }
        /// <summary>
        /// 更换装备库
        /// </summary>
        private bool _ChangeEquipmentByManage;
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool ChangeEquipmentByManage
        {
            get { return _ChangeEquipmentByManage; }
            set { SetField(ref _ChangeEquipmentByManage, value); }
        }

        #endregion 模式设置


        #region 混刷增强
        private bool _MixedbrushReplaceSkill;
        private bool _MixedbrushReplaceEquipment;
 
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool MixedbrushReplaceSkill
        {
            get
            {
                return _MixedbrushReplaceSkill;
            }
            set
            {
                if (_MixedbrushReplaceSkill != value)
                {
                    _MixedbrushReplaceSkill = value;
                    OnPropertyChanged("MixedbrushReplaceSkill");
                }
            }
        }
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool MixedbrushReplaceEquipment
        {
            get
            {
                return _MixedbrushReplaceEquipment;
            }
            set
            {
                if (_MixedbrushReplaceEquipment != value)
                {
                    _MixedbrushReplaceEquipment = value;
                    OnPropertyChanged("MixedbrushReplaceEquipment");
                }
            }
        }
        #endregion 混刷增强

        #region 自动换装备所有变量集
        #region 总开关
        /// <summary>
        /// 职业列表
        /// </summary>
        public List<string> ClassList
        {
            get { return new List<string> { "野蛮人", "圣教军", "魔法师", "巫医", "武僧", "猎魔人", "死灵法师" }; }
        }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ClassListValue { get; set; }

        private bool _OpenReLoad;
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool OpenReLoad
        {
            get
            {
                return _OpenReLoad;
            }
            set
            {
                if (_OpenReLoad != value)
                {
                    _OpenReLoad = value;
                    OnPropertyChanged("OpenReLoad");
                }
            }
        }

        private bool _ReLoadSameNmae;
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool ReLoadSameNmae
        {
            get
            {
                return _ReLoadSameNmae;
            }
            set
            {
                if (_ReLoadSameNmae != value)
                {
                    _ReLoadSameNmae = value;
                    OnPropertyChanged("ReLoadSameNmae");
                }
            }
        }

        private bool _ForBounties;
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool ForBounties
        {
            get
            {
                return _ForBounties;
            }
            set
            {
                if (_ForBounties != value)
                {
                    _ForBounties = value;
                    OnPropertyChanged("ForBounties");
                }
            }
        }
        #endregion

        #region 要更换的部位选择框变量
        private bool _ReLoadHead;
        private bool _ReLoadNeck;
        private bool _ReLoadShoulder;
        private bool _ReLoadHands;
        private bool _ReLoadTorso;
        private bool _ReLoadWrists;
        private bool _ReLoadWaist;
        private bool _ReLoadLegs;
        private bool _ReLoadFeet;
        private bool _ReLoadRingLeft;
        private bool _ReLoadRingRight;
        private bool _ReLoadWeapon;
        private bool _ReLoadOffHand;
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool ReLoadHead
        {
            get
            {
                return _ReLoadHead;
            }
            set
            {
                if (_ReLoadHead != value)
                {
                    _ReLoadHead = value;
                    OnPropertyChanged("ReLoadHead");
                }
            }
        }
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool ReLoadNeck
        {
            get
            {
                return _ReLoadNeck;
            }
            set
            {
                if (_ReLoadNeck != value)
                {
                    _ReLoadNeck = value;
                    OnPropertyChanged("ReLoadNeck");
                }
            }
        }
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool ReLoadShoulder
        {
            get
            {
                return _ReLoadShoulder;
            }
            set
            {
                if (_ReLoadShoulder != value)
                {
                    _ReLoadShoulder = value;
                    OnPropertyChanged("ReLoadShoulder");
                }
            }
        }
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool ReLoadHands
        {
            get
            {
                return _ReLoadHands;
            }
            set
            {
                if (_ReLoadHands != value)
                {
                    _ReLoadHands = value;
                    OnPropertyChanged("ReLoadHands");
                }
            }
        }
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool ReLoadTorso
        {
            get
            {
                return _ReLoadTorso;
            }
            set
            {
                if (_ReLoadTorso != value)
                {
                    _ReLoadTorso = value;
                    OnPropertyChanged("ReLoadTorso");
                }
            }
        }
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool ReLoadWrists
        {
            get
            {
                return _ReLoadWrists;
            }
            set
            {
                if (_ReLoadWrists != value)
                {
                    _ReLoadWrists = value;
                    OnPropertyChanged("ReLoadWrists");
                }
            }
        }
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool ReLoadWaist
        {
            get
            {
                return _ReLoadWaist;
            }
            set
            {
                if (_ReLoadWaist != value)
                {
                    _ReLoadWaist = value;
                    OnPropertyChanged("ReLoadWaist");
                }
            }
        }
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool ReLoadLegs
        {
            get
            {
                return _ReLoadLegs;
            }
            set
            {
                if (_ReLoadLegs != value)
                {
                    _ReLoadLegs = value;
                    OnPropertyChanged("ReLoadLegs");
                }
            }
        }
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool ReLoadFeet
        {
            get
            {
                return _ReLoadFeet;
            }
            set
            {
                if (_ReLoadFeet != value)
                {
                    _ReLoadFeet = value;
                    OnPropertyChanged("ReLoadFeet");
                }
            }
        }
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool ReLoadRingLeft
        {
            get
            {
                return _ReLoadRingLeft;
            }
            set
            {
                if (_ReLoadRingLeft != value)
                {
                    _ReLoadRingLeft = value;
                    OnPropertyChanged("ReLoadRingLeft");
                }
            }
        }
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool ReLoadRingRight
        {
            get
            {
                return _ReLoadRingRight;
            }
            set
            {
                if (_ReLoadRingRight != value)
                {
                    _ReLoadRingRight = value;
                    OnPropertyChanged("ReLoadRingRight");
                }
            }
        }
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool ReLoadWeapon
        {
            get
            {
                return _ReLoadWeapon;
            }
            set
            {
                if (_ReLoadWeapon != value)
                {
                    _ReLoadWeapon = value;
                    OnPropertyChanged("ReLoadWeapon");
                }
            }
        }
        [DataMember(IsRequired = false)]
        [DefaultValue(false)]
        public bool ReLoadOffHand
        {
            get
            {
                return _ReLoadOffHand;
            }
            set
            {
                if (_ReLoadOffHand != value)
                {
                    _ReLoadOffHand = value;
                    OnPropertyChanged("ReLoadOffHand");
                }
            }
        }
        #endregion

        #region 小米+大米装备变量
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadHeadNValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadNeckNValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadShoulderNValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadHandsNValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadTorsoNValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadWristsNValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadWaistNValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadLegsNValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadFeetNValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadRingLeftNValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadRingRightNValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadWeaponNValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadOffHandNValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadHeadGValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadNeckGValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadShoulderGValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadHandsGValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadTorsoGValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadWristsGValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadWaistGValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadLegsGValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadFeetGValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadRingLeftGValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadRingRightGValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadWeaponGValue { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ReLoadOffHandGValue { get; set; }
        #endregion

        #region 小米技能变量
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsN1Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsN2Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsN3Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsN4Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsN5Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsN6Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string PassiveSkillsN1Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string PassiveSkillsN2Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string PassiveSkillsN3Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string PassiveSkillsN4Value { get; set; }
        #endregion

        #region 小米技能符文
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsRunesN1Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsRunesN2Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsRunesN3Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsRunesN4Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsRunesN5Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsRunesN6Value { get; set; }
        #endregion

        #region 大米技能变量
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsG1Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsG2Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsG3Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsG4Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsG5Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsG6Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string PassiveSkillsG1Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string PassiveSkillsG2Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string PassiveSkillsG3Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string PassiveSkillsG4Value { get; set; }
        #endregion

        #region 大米技能符文
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsRunesG1Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsRunesG2Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsRunesG3Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsRunesG4Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsRunesG5Value { get; set; }
        [DataMember(IsRequired = false)]
        [DefaultValue("")]
        public string ActiveSkillsRunesG6Value { get; set; }
        #endregion

        #region 小米+大米装备数据源
        //public  IEnumerable<Item> AllLegendaryList = Legendary.Where(i => i.Name != "");
        public List<string> ReLoadHeadList
        {
            get
            {
                try
                {
                    IEnumerable<Item> AllLegendaryList = Legendary.Where(i => i.Name != "");
                    List<string> list = new List<string>();
                    list = (from u in AllLegendaryList
                            where u.TrinityItemType == TrinityItemType.SpiritStone ||
                            u.TrinityItemType == TrinityItemType.WizardHat ||
                            u.TrinityItemType == TrinityItemType.VoodooMask ||
                            u.TrinityItemType == TrinityItemType.Helm
                            select u.Name).ToList();
                    return list;
                }
                catch
                {
                    return new List<string>();
                }
            }
        }
        public List<string> ReLoadNeckList
        {
            get
            {
                try
                {
                    IEnumerable<Item> AllLegendaryList = Legendary.Where(i => i.Name != "");
                    List<string> list = new List<string>();
                    list = (from u in AllLegendaryList
                            where u.TrinityItemType == TrinityItemType.Amulet
                            select u.Name).ToList();
                    return list;
                }
                catch
                {
                    return new List<string>();
                }
            }
        }
        public List<string> ReLoadShoulderList
        {
            get
            {
                try
                {
                    IEnumerable<Item> AllLegendaryList = Legendary.Where(i => i.Name != "");
                    List<string> list = new List<string>();
                    list = (from u in AllLegendaryList
                            where u.TrinityItemType == TrinityItemType.Shoulder
                            select u.Name).ToList();
                    return list;
                }
                catch
                {
                    return new List<string>();
                }
            }
        }
        public List<string> ReLoadHandsList
        {
            get
            {
                try
                {
                    IEnumerable<Item> AllLegendaryList = Legendary.Where(i => i.Name != "");
                    List<string> list = new List<string>();
                    list = (from u in AllLegendaryList
                            where u.TrinityItemType == TrinityItemType.Gloves
                            select u.Name).ToList();
                    return list;
                }
                catch
                {
                    return new List<string>();
                }
            }
        }
        public List<string> ReLoadTorsoList
        {
            get
            {
                try
                {
                    IEnumerable<Item> AllLegendaryList = Legendary.Where(i => i.Name != "");
                    List<string> list = new List<string>();
                    list = (from u in AllLegendaryList
                            where u.TrinityItemType == TrinityItemType.Chest ||
                            u.TrinityItemType == TrinityItemType.Cloak
                            select u.Name).ToList();
                    return list;
                }
                catch
                {
                    return new List<string>();
                }
            }
        }
        public List<string> ReLoadWristsList
        {
            get
            {
                try
                {
                    IEnumerable<Item> AllLegendaryList = Legendary.Where(i => i.Name != "");
                    List<string> list = new List<string>();
                    list = (from u in AllLegendaryList
                            where u.TrinityItemType == TrinityItemType.Bracer
                            select u.Name).ToList();
                    return list;
                }
                catch
                {
                    return new List<string>();
                }
            }
        }
        public List<string> ReLoadWaistList
        {
            get
            {
                try
                {
                    IEnumerable<Item> AllLegendaryList = Legendary.Where(i => i.Name != "");
                    List<string> list = new List<string>();
                    list = (from u in AllLegendaryList
                            where u.TrinityItemType == TrinityItemType.Belt
                            select u.Name).ToList();
                    return list;
                }
                catch
                {
                    return new List<string>();
                }
            }
        }
        public List<string> ReLoadLegsList
        {
            get
            {
                try
                {
                    IEnumerable<Item> AllLegendaryList = Legendary.Where(i => i.Name != "");
                    List<string> list = new List<string>();
                    list = (from u in AllLegendaryList
                            where u.TrinityItemType == TrinityItemType.Legs
                            select u.Name).ToList();
                    return list;
                }
                catch
                {
                    return new List<string>();
                }
            }
        }
        public List<string> ReLoadFeetList
        {
            get
            {
                try
                {
                    IEnumerable<Item> AllLegendaryList = Legendary.Where(i => i.Name != "");
                    List<string> list = new List<string>();
                    list = (from u in AllLegendaryList
                            where u.TrinityItemType == TrinityItemType.Boots
                            select u.Name).ToList();
                    return list;
                }
                catch
                {
                    return new List<string>();
                }
            }
        }
        public List<string> ReLoadRingLeftList
        {
            get
            {
                try
                {
                    IEnumerable<Item> AllLegendaryList = Legendary.Where(i => i.Name != "");
                    List<string> list = new List<string>();
                    list = (from u in AllLegendaryList
                            where u.TrinityItemType == TrinityItemType.Ring
                            select u.Name).ToList();
                    return list;
                }
                catch
                {
                    return new List<string>();
                }
            }
        }
        public List<string> ReLoadRingRightList
        {
            get
            {
                try
                {
                    IEnumerable<Item> AllLegendaryList = Legendary.Where(i => i.Name != "");
                    List<string> list = new List<string>();
                    list = (from u in AllLegendaryList
                            where u.TrinityItemType == TrinityItemType.Ring
                            select u.Name).ToList();
                    return list;
                }
                catch
                {
                    return new List<string>();
                }
            }
        }
        public List<string> ReLoadWeaponList
        {
            get
            {
                try
                {
                    IEnumerable<Item> AllLegendaryList = Legendary.Where(i => i.Name != "");
                    List<string> list = new List<string>();
                    list = (from u in AllLegendaryList
                            where u.TrinityItemType == TrinityItemType.CeremonialKnife ||
                            u.TrinityItemType == TrinityItemType.Dagger ||
                            u.TrinityItemType == TrinityItemType.Sword ||
                            u.TrinityItemType == TrinityItemType.Axe ||
                            u.TrinityItemType == TrinityItemType.Spear ||
                            u.TrinityItemType == TrinityItemType.Mace ||
                            u.TrinityItemType == TrinityItemType.FistWeapon ||
                            u.TrinityItemType == TrinityItemType.MightyWeapon ||
                            u.TrinityItemType == TrinityItemType.Flail ||
                            u.TrinityItemType == TrinityItemType.TwoHandPolearm ||
                            u.TrinityItemType == TrinityItemType.TwoHandAxe ||
                            u.TrinityItemType == TrinityItemType.TwoHandMace ||
                            u.TrinityItemType == TrinityItemType.TwoHandFlail ||
                            u.TrinityItemType == TrinityItemType.TwoHandDaibo ||
                            u.TrinityItemType == TrinityItemType.TwoHandSword ||
                            u.TrinityItemType == TrinityItemType.TwoHandMighty ||
                            u.TrinityItemType == TrinityItemType.TwoHandStaff ||
                            u.TrinityItemType == TrinityItemType.TwoHandBow ||
                            u.TrinityItemType == TrinityItemType.TwoHandCrossbow ||
                            u.TrinityItemType == TrinityItemType.HandCrossbow ||
                            u.TrinityItemType == TrinityItemType.Wand ||
                            u.TrinityItemType == TrinityItemType.Scythe ||
                            u.TrinityItemType == TrinityItemType.TwoHandScythe
                            select u.Name).ToList();
                    return list;
                }
                catch
                {
                    return new List<string>();
                }
            }
        }
        public List<string> ReLoadOffHandList
        {
            get
            {
                try
                {
                    IEnumerable<Item> AllLegendaryList = Legendary.Where(i => i.Name != "");
                    List<string> list = new List<string>();
                    list = (from u in AllLegendaryList
                            where u.TrinityItemType == TrinityItemType.CrusaderShield ||
                            u.TrinityItemType == TrinityItemType.Dagger ||
                            u.TrinityItemType == TrinityItemType.Sword ||
                            u.TrinityItemType == TrinityItemType.Axe ||
                            u.TrinityItemType == TrinityItemType.Spear ||
                            u.TrinityItemType == TrinityItemType.Mace ||
                            u.TrinityItemType == TrinityItemType.FistWeapon ||
                            u.TrinityItemType == TrinityItemType.MightyWeapon ||
                            u.TrinityItemType == TrinityItemType.Flail ||
                            u.TrinityItemType == TrinityItemType.Orb ||
                            u.TrinityItemType == TrinityItemType.Mojo ||
                            u.TrinityItemType == TrinityItemType.Quiver ||
                            u.TrinityItemType == TrinityItemType.Shield ||
                            u.TrinityItemType == TrinityItemType.HandCrossbow ||
                            u.TrinityItemType == TrinityItemType.Wand ||
                            u.TrinityItemType == TrinityItemType.Phylactery
                            select u.Name).ToList();
                    return list;
                }
                catch
                {
                    return new List<string>();
                }
            }
        }
        #endregion

        #region 技能源
        public List<string> ActiveSkillsList
        {
            get
            {
                List<string> list = new List<string>();
                if (ClassListValue == "野蛮人")
                {
                    list = Skills.Barbarian.Where(i => i.Name != "").Select(i => i.Name).ToList();
                }
                else if (ClassListValue == "圣教军")
                {
                    list = Skills.Crusader.Where(i => i.Name != "").Select(i => i.Name).ToList();
                }
                else if (ClassListValue == "魔法师")
                {
                    list = Skills.Wizard.Where(i => i.Name != "").Select(i => i.Name).ToList();
                }
                else if (ClassListValue == "巫医")
                {
                    list = Skills.WitchDoctor.Where(i => i.Name != "").Select(i => i.Name).ToList();
                }
                else if (ClassListValue == "武僧")
                {
                    list = Skills.Monk.Where(i => i.Name != "").Select(i => i.Name).ToList();
                }
                else if (ClassListValue == "猎魔人")
                {
                    list = Skills.DemonHunter.Where(i => i.Name != "").Select(i => i.Name).ToList();
                }
                else if (ClassListValue == "死灵法师")
                {
                    list = Skills.Necromancer.Where(i => i.Name != "").Select(i => i.Name).ToList();
                }
                return list;
            }
        }
        public List<string> PassiveSkillsList
        {
            get
            {
                List<string> list = new List<string>();
                if (ClassListValue == "野蛮人")
                {
                    list = Passives.Barbarian.Where(i => i.Name != "").Select(i => i.Name).ToList();
                }
                else if (ClassListValue == "圣教军")
                {
                    list = Passives.Crusader.Where(i => i.Name != "").Select(i => i.Name).ToList();
                }
                else if (ClassListValue == "魔法师")
                {
                    list = Passives.Wizard.Where(i => i.Name != "").Select(i => i.Name).ToList();
                }
                else if (ClassListValue == "巫医")
                {
                    list = Passives.WitchDoctor.Where(i => i.Name != "").Select(i => i.Name).ToList();
                }
                else if (ClassListValue == "武僧")
                {
                    list = Passives.Monk.Where(i => i.Name != "").Select(i => i.Name).ToList();
                }
                else if (ClassListValue == "猎魔人")
                {
                    list = Passives.DemonHunter.Where(i => i.Name != "").Select(i => i.Name).ToList();
                }
                else if (ClassListValue == "死灵法师")
                {
                    list = Passives.Necromancer.Where(i => i.Name != "").Select(i => i.Name).ToList();
                }
                list.Insert(0, "无");
                return list;
            }

        }
        #endregion

        #region 通过识别职业来获取符文数据源
        public List<string> Dates(string str)
        {
            Skill skill = null;

            if (ClassListValue == "野蛮人")
            {
                skill = Skills.Barbarian.Where(i => i.Name == str).FirstOrDefault();
            }
            else if (ClassListValue == "圣教军")
            {
                skill = Skills.Crusader.Where(i => i.Name == str).FirstOrDefault();
            }
            else if (ClassListValue == "魔法师")
            {
                skill = Skills.Wizard.Where(i => i.Name == str).FirstOrDefault();
            }
            else if (ClassListValue == "巫医")
            {
                skill = Skills.WitchDoctor.Where(i => i.Name == str).FirstOrDefault();
            }
            else if (ClassListValue == "武僧")
            {
                skill = Skills.Monk.Where(i => i.Name == str).FirstOrDefault();
            }
            else if (ClassListValue == "猎魔人")
            {
                skill = Skills.DemonHunter.Where(i => i.Name == str).FirstOrDefault();
            }
            else if (ClassListValue == "死灵法师")
            {
                skill = Skills.Necromancer.Where(i => i.Name == str).FirstOrDefault();
            }
            List<string> list = new List<string>();
            if (skill != null)
            {
                list = skill.Runes.Select(i => i.Name).ToList();
            }
            return list;
        }

        #endregion

        #region 符文源

        #region 小米技能符文源
        public List<string> ActiveSkillsRunesN1List
        {
            get
            {

                return Dates(ActiveSkillsN1Value);
            }

        }
        public List<string> ActiveSkillsRunesN2List
        {
            get
            {
                return Dates(ActiveSkillsN2Value);
            }
        }
        public List<string> ActiveSkillsRunesN3List
        {
            get
            {
                return Dates(ActiveSkillsN3Value);
            }
        }
        public List<string> ActiveSkillsRunesN4List
        {
            get
            {
                return Dates(ActiveSkillsN4Value);
            }
        }
        public List<string> ActiveSkillsRunesN5List
        {
            get
            {
                return Dates(ActiveSkillsN5Value);
            }
        }
        public List<string> ActiveSkillsRunesN6List
        {
            get
            {
                return Dates(ActiveSkillsN6Value);
            }
        }
        #endregion

        #region 大米技能符文源
        public List<string> ActiveSkillsRunesG1List
        {
            get
            {
                return Dates(ActiveSkillsG1Value);
            }
        }
        public List<string> ActiveSkillsRunesG2List
        {
            get
            {
                return Dates(ActiveSkillsG2Value);
            }
        }
        public List<string> ActiveSkillsRunesG3List
        {
            get
            {
                return Dates(ActiveSkillsG3Value);
            }
        }
        public List<string> ActiveSkillsRunesG4List
        {
            get
            {
                return Dates(ActiveSkillsG4Value);
            }
        }
        public List<string> ActiveSkillsRunesG5List
        {
            get
            {
                return Dates(ActiveSkillsG5Value);
            }
        }
        public List<string> ActiveSkillsRunesG6List
        {
            get
            {
                return Dates(ActiveSkillsG6Value);
            }
        }
        #endregion

        #endregion

        #endregion


        #region 函数
        

            
        #endregion
    }
}
