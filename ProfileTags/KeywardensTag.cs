﻿using System.Linq;
using Trinity.Framework;
using System.Threading.Tasks;
using Trinity.Components.Adventurer.Coroutines.KeywardenCoroutines;
using Trinity.Components.Adventurer.Game.Events;
using Trinity.Components.QuestTools;
using Zeta.Bot;
using Zeta.Bot.Profile;
using Zeta.Game;
using Zeta.TreeSharp;
using Zeta.XmlEngine;

namespace Trinity.ProfileTags
{
    [XmlElement("Keywardens")]
    public class KeywardensTag : BaseProfileBehavior
    {
        private KeywardenCoroutine _keywardenCoroutine;

        public override async Task<bool> StartTask()
        {
            PluginEvents.CurrentProfileType = ProfileType.Keywarden;

            var keywardenData = GetNext();
            if (keywardenData == null)
            {
                Core.Logger.Log("[钥匙守护者] 钥匙守护者全部消灭,重新开始游戏.");
                return true;
            }

            _keywardenCoroutine = new KeywardenCoroutine(keywardenData);
            return false;
        }

        public override async Task<bool> MainTask()
        {
            if (!await _keywardenCoroutine.GetCoroutine())
                return false;

            _keywardenCoroutine.Dispose();
            _keywardenCoroutine = null;

            var keywardenData = GetNext();
            if (keywardenData != null)
            {
                _keywardenCoroutine = new KeywardenCoroutine(keywardenData);
                return false;
            }

            return true;
        }

        private KeywardenData GetNext()
        {
            Core.Logger.Log("[钥匙守护者]最新装置数量");
            Core.Logger.Log("[钥匙守护者] Act 1 悔恨之炼狱装置: {0}", KeywardenDataFactory.Items[Act.A1].MachinesCount);
            Core.Logger.Log("[钥匙守护者] Act 2 腐臭之炼狱装置: {0}", KeywardenDataFactory.Items[Act.A2].MachinesCount);
            Core.Logger.Log("[钥匙守护者] Act 3 恐惧之炼狱装置: {0}", KeywardenDataFactory.Items[Act.A3].MachinesCount);
            Core.Logger.Log("[钥匙守护者] Act 4 惊悚之炼狱装置: {0}", KeywardenDataFactory.Items[Act.A4].MachinesCount);
            var averageMachinesCount = KeywardenDataFactory.Items.Values.Average(kwd => kwd.MachinesCount);
            return KeywardenDataFactory.Items.Values.OrderBy(kwd => kwd.MachinesCount).FirstOrDefault(kwd => kwd.IsAlive && kwd.MachinesCount <= averageMachinesCount + 1);
        }
    }
}